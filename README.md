# xor-encryption

Simple XOR encryption for the contents of a file. Takes an input file, an output file, and a user defined key. Originally compiled on Debian 9 with GCC.
