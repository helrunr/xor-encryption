#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int  main(int argc, char* argv[3])
{
	if (argc == 0)
	{
		printf("\n [input file] [output file] [key]\n");
	}
	
	FILE* source;
	FILE* output;
	char buf[1024];
	int key = 0;
	int byte;

	source	= fopen(argv[1], "r");
	output	= fopen(argv[2], "w");

	while ((byte = fgetc(source)) != EOF)
	{
		fputc(byte ^ argv[3][key], output);
		key++;

		if (key == strlen(argv[3]))
		{
			key = 0;
		}
	}
	
	printf("Completed.");

	return 0;
}
